//
//  SeniorExplorerCollectionViewCell.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 21/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class SeniorExplorersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var seniorExplorerImageView: UIImageView!
    @IBOutlet weak var seniorExplorerNameLabel: UILabel!
    @IBOutlet weak var seniorExplorerRoleLabel: UILabel!
    
    func setExplorer(explorer: Explorer) {
        seniorExplorerImageView.image = explorer.image
        seniorExplorerNameLabel.text = explorer.name
        seniorExplorerRoleLabel.text = explorer.role
    }
}
