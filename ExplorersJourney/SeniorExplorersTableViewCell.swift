//
//  SeniorExplorersTableViewCell.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class SeniorExplorersTableViewCell: UITableViewCell {
    @IBOutlet weak var seniorExplorerImageView: UIImageView!
    @IBOutlet weak var seniorExplorerNameLabel: UILabel!
    @IBOutlet weak var seniorExplorerRoleLabel: UILabel!
    
func setSeniorExplorer(seniorExplorer: Explorer) {
    seniorExplorerImageView.image = seniorExplorer.image
    seniorExplorerNameLabel.text = seniorExplorer.name
    seniorExplorerRoleLabel.text = seniorExplorer.role
    }
}
