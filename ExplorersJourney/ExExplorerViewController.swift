//
//  ExExplorerViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class ExExplorerViewController: UIViewController {
    @IBOutlet weak var explorerImageView: UIImageView!
    @IBOutlet weak var explorerNameLabel: UILabel!
    @IBOutlet weak var explorerRoleLabel: UILabel!
    @IBOutlet weak var explorerHelpLabel: UILabel!
    @IBOutlet weak var explorerLearnLabel: UILabel!
    
    var explorerImage = #imageLiteral(resourceName: "challenge6")
    var explorerName = ""
    var explorerRole = ""
    var explorerHelp = ""
    var explorerLearn = ""
    
    var explorers: [Explorer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        explorerImageView.image = explorerImage
        explorerNameLabel.text = explorerName
        explorerRoleLabel.text = explorerRole
        explorerHelpLabel.text = explorerHelp
        explorerLearnLabel.text = explorerLearn
        
        explorers = createExplorerArray()
    }
    
    func createExplorerArray() -> [Explorer]{
        var tempExplorer: [Explorer] = []
        var tempSE: [Explorer] = []
        var tempMS: [Explorer] = []
        var tempAS: [Explorer] = []
        
        for index in 0...SeniorExplorersData.seniorExplorers.count - 1 {
            tempMS.append(SeniorExplorersData.seniorExplorers[index])
            
        }
        
        for index in 0...JuniorExplorersMSData.juniorExplorersMS.count - 1 {
            tempSE.append(JuniorExplorersMSData.juniorExplorersMS[index])
            
        }
        
        for index in 0...JuniorExplorersASData.juniorExplorersAS.count - 1 {
            tempAS.append(JuniorExplorersASData.juniorExplorersAS[index])
        }
        
        tempExplorer = tempSE + tempMS + tempAS
        
        return tempExplorer
    }
}
