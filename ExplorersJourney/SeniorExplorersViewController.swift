//
//  SeniorExplorersViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

//
//  AppViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 17/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class SeniorExplorersViewController: UIViewController {
    @IBOutlet weak var seniorExplorersTableView: UITableView!
    @IBOutlet weak var seniorExplorersSearchBar: UISearchBar!
    
    var seArray: [Explorer] = []
    var selectedIndex = Int()
    
    var searchExplorer: [Explorer] = []
    var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        seArray = createSeniorExplorerArray()
        searchExplorer = createSeniorExplorerArray()
        
        seniorExplorersTableView.dataSource = self
        seniorExplorersTableView.delegate = self
        seniorExplorersSearchBar.delegate = self
    }
    
    func createSeniorExplorerArray() -> [Explorer]{
        var tempExplorer: [Explorer] = []
        
        for index in 0...SeniorExplorersData.seniorExplorers.count - 1 {
            tempExplorer.append(SeniorExplorersData.seniorExplorers[index])
            
        }
        
        return tempExplorer
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        performSegue(withIdentifier: "ExpandSeniorExplorer", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ExpandSeniorExplorer"{
            let exExplorerVC: ExExplorerViewController = segue.destination as! ExExplorerViewController
            
            if searching {
                exExplorerVC.explorerImage = searchExplorer[selectedIndex].image
                exExplorerVC.explorerName =  searchExplorer[selectedIndex].name
                exExplorerVC.explorerRole = searchExplorer[selectedIndex].role
                exExplorerVC.explorerHelp = searchExplorer[selectedIndex].help
                exExplorerVC.explorerLearn = searchExplorer[selectedIndex].learn
            } else {
                exExplorerVC.explorerImage = seArray[selectedIndex].image
                exExplorerVC.explorerName =  seArray[selectedIndex].name
                exExplorerVC.explorerRole = seArray[selectedIndex].role
                exExplorerVC.explorerHelp = seArray[selectedIndex].help
                exExplorerVC.explorerLearn = seArray[selectedIndex].learn
            }
        }
    }
}

extension SeniorExplorersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchExplorer.count
        } else {
            return seArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let seniorExplorers = seArray[indexPath.row]
        let searchSeniorExplorers = searchExplorer[indexPath.row]
        
        let cell = seniorExplorersTableView.dequeueReusableCell(withIdentifier: "SeniorExplorersTableViewCell") as! SeniorExplorersTableViewCell
        
        if searching {
            cell.setSeniorExplorer(seniorExplorer: searchSeniorExplorers)
        } else {
            cell.setSeniorExplorer(seniorExplorer: seniorExplorers)
        }
        
        return cell
    }
    
}

extension SeniorExplorersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchExplorer = seArray.filter ({ $0.name.lowercased().prefix(searchText.count) == searchText.lowercased() })
        searching = true
        seniorExplorersTableView.reloadData()
    }
}

