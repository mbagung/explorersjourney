//
//  SeniorExplorerCollectionViewCell.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 21/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class JuniorExplorersMSCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var juniorExplorerMSImageView: UIImageView!
    @IBOutlet weak var juniorExplorerMSNameLabel: UILabel!
    @IBOutlet weak var juniorExplorerMSRoleLabel: UILabel!
    
    func setExplorer(explorer: Explorer) {
        juniorExplorerMSImageView.image = explorer.image
        juniorExplorerMSNameLabel.text = explorer.name
        juniorExplorerMSRoleLabel.text = explorer.role
    }
}
