//
//  ExChallengeViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 20/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class ExChallengeViewController: UIViewController {
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var challengeTitleLabel: UILabel!
    @IBOutlet weak var challengeCategoryLabel: UILabel!
    @IBOutlet weak var challengeDurationLabel: UILabel!
    @IBOutlet weak var challengeDescriptionLabel: UILabel!
    @IBOutlet weak var challengeLearningGoalsLabel: UILabel!
    @IBOutlet weak var challengeConstraintsLabel: UILabel!
    @IBOutlet weak var challengeDeliverablesLabel: UILabel!
    
    
    var challengeImage = #imageLiteral(resourceName: "challenge6")
    var challengeTitle = ""
    var challengeCategory = ""
    var challengeDuration = ""
    var challengeDescription = ""
    var challengeLearningGoals = ""
    var challengeConstraints = ""
    var challengeDeliverables = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        challengeImageView.image = challengeImage
        challengeTitleLabel.text = challengeTitle
        challengeCategoryLabel.text = challengeCategory
        challengeDurationLabel.text = challengeDuration
        challengeDescriptionLabel.text = challengeDescription
        challengeLearningGoalsLabel.text = challengeLearningGoals
        challengeConstraintsLabel.text = challengeConstraints
        challengeDeliverablesLabel.text = challengeDeliverables
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
