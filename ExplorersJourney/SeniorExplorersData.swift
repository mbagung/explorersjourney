//
//  SeniorExplorersData.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

struct SeniorExplorersData {
    
    static var seniorExplorers: [Explorer] =
    [
        Explorer(name: "Dickson Leonard", seniority: "Senior", shift: "All", role: "Academy Manager", image: #imageLiteral(resourceName: "Dickson_SE" ), learn: "", help: "Coding basics (algorithm, data structure, operations)\nApp development process (problem definition, ideation, prototyping, production), Basic UX, Professional skills (leadership, communication)"),
        Explorer(name: "Fanny Halim", seniority: "Senior", shift: "All", role: "Coding Facilitator", image: #imageLiteral(resourceName: "Fanny_SE" ), learn: "English Speaking", help: "Basic iOS Development\n(Swift, XCode, Auto Layout, Data Collection)\nDiscussion"),
        Explorer(name: "Januar Tanzil", seniority: "Senior", shift: "All", role: "Coding Facilitator", image: #imageLiteral(resourceName: "Januar_SE" ), learn: "", help: "Swift language, IOS Development, Game Development, Software Engineering in general"),
        Explorer(name: "Jaya Pranata", seniority: "Senior", shift: "All", role: "Coding Facilitator", image: #imageLiteral(resourceName: "Jaya_SE" ), learn: "Graphic Design (layout/composition, typography, branding), UI/UX, problem analysis, basic research, HIG", help: "Coding basic, Swift Language, Auto Layout, Core Data, Map"),
        Explorer(name: "Rachmat Kukuh Rahadiansyah", seniority: "Senior", shift: "All", role: "Coding Facilitator", image: #imageLiteral(resourceName: "Kukuh_SE" ), learn: "", help: "- General iOS app development\n- iOS frameworks\n- Swift language"),
        Explorer(name: "Yulibar Husni", seniority: "Senior", shift: "All", role: "Coding Facilitator", image: #imageLiteral(resourceName: "Yulibar_SE" ), learn: "- Ideation\n- English Speaking\n- Project Management", help: "- Swift \n- Ideation\n- Animation production"),
        Explorer(name: "Ari Kurniawan", seniority: "Senior", shift: "All", role: "Design Facilitator", image: #imageLiteral(resourceName: "Ari_SE"), learn: "Basic coding", help: "Graphic Design (layout/composition, typography, branding), HIG, UI/UX"),
        Explorer(name: "Bayu Prasetya", seniority: "Senior", shift: "All", role: "Design Facilitator", image: #imageLiteral(resourceName: "Bayu_SE"), learn: "", help: "Visual Branding, Design Thinking, Development Process, UI/UX, Nemenin Mikir, Makan-makan (soon)"),
        Explorer(name: "Gabriele Wijasa", seniority: "Senior", shift: "All", role: "Design Facilitator", image: #imageLiteral(resourceName: "Gabriele_SE"), learn: "", help: "Graphic Design, UI/UX, Photography"),
        Explorer(name: "Yehezkiel Cheryan Tjandra", seniority: "Senior", shift: "All", role: "Professional Facilitator", image: #imageLiteral(resourceName: "Ryan_SE"), learn: "Swift/Xcode in general", help: "English language, Public speaking, Business presentation, Business writing, Business insights"),
        Explorer(name: "John Alan Ketaren", seniority: "Senior", shift: "All", role: "Professional Facilitator", image: #imageLiteral(resourceName: "Ketaren_SE"), learn: "Basic Xcode", help: "Ideation, bringing resources, user testing, business, fintech, Presentation,Networking."),
    ]
}
