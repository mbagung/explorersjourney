//
//  SeniorExplorerCollectionViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 21/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class SeniorExplorerCollectionViewController: UICollectionViewController {
    
    var explorers: [Explorer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension SeniorExplorerCollectionViewController: UICollectionViewDataSource {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return explorers
    }
}
