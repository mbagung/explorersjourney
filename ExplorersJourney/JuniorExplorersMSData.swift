//
//  JuniorExplorersMSData.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

struct JuniorExplorersMSData {
    static var juniorExplorersMS: [Explorer] =
    [
        Explorer(name: "Amelia Verina Siswanto", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Amelia_MO"), learn: "Swift, UI/UX Design, Public Speaking, English Practice", help: "Design, Interior & Architecture Design."),
        Explorer(name: "Angela Priscila Montolalu", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Angel_MO" ), learn: "UI/UX Design, Swift, English Speaking", help: "Graphic Design (Branding)"),
        Explorer(name: "Dionesia Nadya Dewayani", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Nadya_MO"), learn: "", help: "Illustration"),
        Explorer(name: "Ferdinan Linardi", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Ferdinan_MO"), learn: "Swift, UI/UX Design, Copywriting, Public Speaking, English Practice", help: "Ilustration, Graphic Design, Fashion Styling"),
        Explorer(name: "Geraldine Janice Wibisono", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Janice_MO"), learn: "Intermediate coding, translating design into code", help: "Graphic design, Illustration"),
        Explorer(name: "Hana Christina Rondoh", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Hana_MO"), learn: "XCode, Swift, UI/UX Design, English Practice, English Public Speaking, English copywriting", help: "Fashion design, Graphic design, Fashion business and styling"),
        Explorer(name: "Henry Christanto Sutjiono", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Henry_MO"), learn: "Basic Coding for XCode, designing prototype with Sketch", help: "Photography, Copywriting & Advertising"),
        Explorer(name: "Jesslyn Faustina", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Jesslyn_MO"), learn: "Intermediate coding", help: "Graphic Design"),
        Explorer(name: "Josephine Sugiharto", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Jose_MO"), learn: "Basic Coding", help: "Branding, Graphic Design"),
        Explorer(name: "Rachel Audrey Effendi", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Rachel_MO"), learn: "Basic coding", help: "Graphic Design, Branding"),
        Explorer(name: "Siti Istiany Tribunda", seniority: "Junior", shift: "Morning", role: "Design", image: #imageLiteral(resourceName: "Isti_MO"), learn: "-English speaking\n-Public speaking\n-Team work\n- Coding, Swift, UI&UX Design.", help: "Graphic Design :\nBranding\nLayout\n\n\nSocial Media Enthusiast :p"),
        Explorer(name: "Baby Amelia Andina Putri", seniority: "Junior", shift: "Morning", role: "Other", image: #imageLiteral(resourceName: "Baby_MO"), learn: "- Swift \n- xCode\n- UI/UX design", help: "Business insights, public speaking and presentation skill, basic design."),
        Explorer(name: "James Tirto", seniority: "Junior", shift: "Morning", role: "Other", image: #imageLiteral(resourceName: "James_MO"), learn: "", help: ""),
        Explorer(name: "Karina Enny Agustina", seniority: "Junior", shift: "Morning", role: "Other", image: #imageLiteral(resourceName: "Karina_MO"), learn: "", help: ""),
        Explorer(name: "Leonora Jeanifer", seniority: "Junior", shift: "Morning", role: "Other", image: #imageLiteral(resourceName: "Jean_MO"), learn: "", help: ""),
        Explorer(name: "Yonatan Niko Sucahyo", seniority: "Junior", shift: "Morning", role: "Other", image: #imageLiteral(resourceName: "Niko_MO"), learn: "basic coding, graphic Design", help: "Business Management"),
        Explorer(name: "Ahmad Rizki Maulana", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Rizki_MO"), learn: "", help: "Coding Basic"),
        Explorer(name: "Albert Pangestu", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Albert_MO"), learn: "Ideation.\nSpeak English.\nUI and UX Design.", help: "Programming Basics."),
        Explorer(name: "Alifudin Aziz", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Aziz_MO"), learn: "", help: ""),
        Explorer(name: "Ardy Stephanus", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Ardy_MO"), learn: "", help: "Basic Coding, Basic Swift"),
        Explorer(name: "Arifin Firdaus", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Arifin_MO"), learn: "", help: "iOS Development (UIKit), Clean Code"),
        Explorer(name: "Arnold Pangestu", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Arnold_MO"), learn: "", help: "Coding, Basic UI/UX Game Design, Unity;"),
        Explorer(name: "Audhy Virabri Kressa", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Audhy_MO"), learn: "", help: "- basic wift\n- git version controller\n- parsing data"),
        Explorer(name: "Bagus Setiawan", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Bagus_MO"), learn: "", help: ""),
        Explorer(name: "Benyamin Rondang Tuahta", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Ben_MO"), learn: "Leading & trailing swipe action", help: "Basic coding"),
        Explorer(name: "Dinie P Hemas", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Dinie_MO"), learn: "Public speaking, English", help: "Basic coding, UI pattern"),
        Explorer(name: "Edward da Costa", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Edward_MO"), learn: "Design , Advance Code", help: "Basic Code"),
        Explorer(name: "Faris Rasyadi Putra", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Faris_MO"), learn: "", help: "Unity 3D Programmer"),
        Explorer(name: "Ibrahim Yunus Muhammad Fiqhan", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Fiqhan_MO"), learn: "English, Graphic Design, Custom Component in Swift (Star Rating)", help: "Basic programing, Object Oriented Programing (OOP), Software Design Pattern"),
        Explorer(name: "Janice Budihartono", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "J'Ice_MO"), learn: "Auto layout, Intermediate Swift Code (maybe), Design", help: "- Code\n- English"),
        Explorer(name: "Jeffrey Phinardi Kosasih", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Jep_MO"), learn: "Public Speaking, Xcode (Constraint, Chat Feature, and Verification Code)", help: "Youtube content, Pr, Ph, Ai, Basic coding,"),
        Explorer(name: "Jeffry Sandy Purnomo", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Jeffry_MO"), learn: "Swift UI", help: "Basic Code, Basic Sketch"),
        Explorer(name: "Lois Pangestu", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Lois_MO"), learn: "", help: "Basic Coding, Basic Swift, Idea"),
        Explorer(name: "M. Rizky Hidayat", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Rizky_MO"), learn: "- English speak\n- Swift code style\n- Self management", help: "Basic Code, SysAdmin"),
        Explorer(name: "Martin Ivo Hardinoto", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Martin_MO"), learn: "", help: "- Basic swift\n\n\n- Ask me everything about covid-19"),
        Explorer(name: "Michael Randicha Gunawan Santoso", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Michael_MO"), learn: "Someone with Intermediate / Advanced understanding on TableViewController, TableViewCell, InputAccessoryView, TableViewCell's item's constraint & Protocol", help: "Coder : Basic Swift, Intermediate Javascript (Angular & Ionic), Intermediate Laravel, Interested in Flutter"),
        Explorer(name: "Moh. Zinnur Atthufail Addausi", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "A'il_MO"), learn: "", help: ""),
        Explorer(name: "Najibullah Ulul Albab", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Ulul_MO"), learn: "Swift Intermediate, UX Designer", help: "Code, UI/UX Pattern"),
        Explorer(name: "Natanael Geraldo Santoso", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Natanael_MO"), learn: "", help: ""),
        Explorer(name: "Nicholas Ang", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Nicholas_MO"), learn: "", help: "Videography, Photography, Game Development (Unity), Animation and VFX, Basic Code"),
        Explorer(name: "Nur Afni Zuhrotul Laili", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Afni_MO"), learn: "", help: "- basic swift\n- passing data between viewcontroller\n- set onboarding screen"),
        Explorer(name: "Oktavia Citra Resmi Rachmawati", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Citra_MO"), learn: "", help: ""),
        Explorer(name: "Rizal Hidayat", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Rizal_MO"), learn: "English", help: "Basic code / basic swift"),
        Explorer(name: "Steven Wijaya", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "SW_MO"), learn: "", help: "Basic Code, Basic Swift, iOS dev, a bit MacOS dev, Laravel, Backend, etc."),
        Explorer(name: "Taufiq Ramadhany", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Taufiq_MO"), learn: "", help: "Basic swift"),
        Explorer(name: "Tony Varian Yoditanto", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Tony_MO"), learn: "", help: ""),
        Explorer(name: "Wahyu Kharisma Mujiono", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Wahyujus_MO"), learn: "", help: ""),
        Explorer(name: "Wahyu Saputra", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Wahyu_MO"), learn: "Photoeditor, English Speaking", help: "Basic Coding"),
        Explorer(name: "Winata Arafat Fal Aham", seniority: "Junior", shift: "Morning", role: "Tech", image: #imageLiteral(resourceName: "Badai_MO"), learn: "UI/UX Design, Swift, English Speaking", help: "Photography, Basic Coding, Critical Thinking, git,"),
    ]
}
