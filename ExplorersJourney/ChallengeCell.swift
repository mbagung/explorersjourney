//
//  ChallengeCell.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 17/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class ChallengeCell: UITableViewCell {
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var challengeTitleLabel: UILabel!
    @IBOutlet weak var challengeCategoryLabel: UILabel!
    @IBOutlet weak var challengeDurationLabel: UILabel!
    
    func setChallenge(challenge: Challenge) {
        challengeImageView.image = challenge.image
        challengeTitleLabel.text = challenge.title
        challengeCategoryLabel.text = challenge.category
        challengeDurationLabel.text = challenge.duration
    }
}
