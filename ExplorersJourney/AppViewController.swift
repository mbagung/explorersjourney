//
//  AppViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 17/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

var challenges: [Challenge] = []

class AppViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        challenges = createChallengeArray()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func createChallengeArray() -> [Challenge]{
        var tempChallenge: [Challenge] = []
        
        let challenge1 = Challenge(image: #imageLiteral(resourceName: "challenge1"), title: "Challenge 1", category: "Group Challenge", duration: "5 Weeks", description: "No Game\nNo Back Ends\nNo Augmented Reality\n", learningGoals: "Full Cycle CBL\nHIG (Human Interface Guidelines)\nPrototyping Xcode & Basic Swift\nGroup Collaboration", constraints: "No Game\nNo Back Ends\nNo Augmented Reality\n", deliverables: "Working prototype")
        let challenge2 = Challenge(image: #imageLiteral(resourceName: "challenge2"), title: "Challenge 2", category: "Individual Challenge", duration: "3 Weeks", description: "Create an Explorer's Journey App. You can add as many features and functions as you'd like. Just consider it's functionalities. Be as creative as possible.", learningGoals: "This is individual work\nNo\nthird-party library\nUse HIG interfaces essentials", constraints: "", deliverables: "Sketch Prototype\nLearning Backlog Document\nXcode Project\nFeedback Report Document")
        let challenge3 = Challenge(image: #imageLiteral(resourceName: "challenge3"), title: "Challenge 3", category: "Group Challenge", duration: "4 Weeks", description: "TBA", learningGoals: "Collaboration\nBest Practice Coding\nUnderstanding User\nBest Practice Project Management", constraints: "No Constraints", deliverables: "User narration\nTrello for project\nWorking app with clean code")
        let challenge4 = Challenge(image: #imageLiteral(resourceName: "challenge4"), title: "Challenge 4", category: "Group Challenge", duration: "4 Weeks", description: "TBA", learningGoals: "TBA", constraints: "TBA", deliverables: "TBA")
        let challenge5 = Challenge(image: #imageLiteral(resourceName: "challenge5"), title: "Challenge 5", category: "Individual Challenge", duration: "2 Weeks", description: "TBA", learningGoals: "TBA", constraints: "TBA", deliverables: "TBA")
        let challenge6 = Challenge(image: #imageLiteral(resourceName: "challenge6"), title: "Macro Challenge", category: "Group Challenge", duration: "12 Weeks", description: "TBA", learningGoals: "TBA", constraints: "TBA", deliverables: "TBA")
        
        tempChallenge.append(challenge1)
        tempChallenge.append(challenge2)
        tempChallenge.append(challenge3)
        tempChallenge.append(challenge4)
        tempChallenge.append(challenge5)
        tempChallenge.append(challenge6)
        
        return tempChallenge
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        performSegue(withIdentifier: "ExpandChallenge", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ExpandChallenge"{
            let exChallengeVC : ExChallengeViewController = segue.destination as! ExChallengeViewController
            
            exChallengeVC.challengeImage = challenges[selectedIndex].image
            exChallengeVC.challengeTitle = challenges[selectedIndex].title
            exChallengeVC.challengeCategory = challenges[selectedIndex].category
            exChallengeVC.challengeDuration = challenges[selectedIndex].duration
            exChallengeVC.challengeDescription = challenges[selectedIndex].description
            exChallengeVC.challengeLearningGoals = challenges[selectedIndex].learningGoals
            exChallengeVC.challengeConstraints = challenges[selectedIndex].constraints
            exChallengeVC.challengeDeliverables = challenges[selectedIndex].deliverables
        }
        
        
    }

}

extension AppViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return challenges.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let challenge = challenges[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChallengeCell") as! ChallengeCell
        
        cell.setChallenge(challenge: challenge)
        
        return cell
    }
    
}
