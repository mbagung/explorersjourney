//
//  JuniorExplorersASCollectionViewCell.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class JuniorExplorersASCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var juniorExplorerASImageView: UIImageView!
    @IBOutlet weak var juniorExplorerASNameLabel: UILabel!
    @IBOutlet weak var juniorExplorerASRoleLabel: UILabel!
    
    func setExplorer(explorer: Explorer) {
        juniorExplorerASImageView.image = explorer.image
        juniorExplorerASNameLabel.text = explorer.name
        juniorExplorerASRoleLabel.text = explorer.role
    }
}
