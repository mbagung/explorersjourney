//
//  Challenge.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 17/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class Explorer {
    var name: String
    var seniority: String
    var shift: String
    var role: String
    var image: UIImage
    var learn: String
    var help: String
    
    init(name: String, seniority: String, shift:String, role: String, image: UIImage, learn: String, help: String) {
        self.name = name
        self.seniority = seniority
        self.shift = shift
        self.role = role
        self.image = image
        self.learn = learn
        self.help = help
    }
}
