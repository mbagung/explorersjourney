//
//  ExplorersViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 21/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class ExplorersViewController: UIViewController {
    @IBOutlet weak var seniorExplorersCollectionView: UICollectionView!
    @IBOutlet weak var juniorExplorersMSCollectionView: UICollectionView!
    @IBOutlet weak var juniorExplorersASCollectionView: UICollectionView!
    
    var seArray: [Explorer] = []
    var jmsArray: [Explorer] = []
    var jasArray: [Explorer] = []
    
    override func viewDidLoad() {
        
        seArray = createSeniorExplorerArray()
        jmsArray = createJuniorExplorerMSArray()
        jasArray = createJuniorExplorerASArray()
        
        seniorExplorersCollectionView.dataSource = self
        juniorExplorersMSCollectionView.dataSource = self
        juniorExplorersASCollectionView.dataSource = self
        seniorExplorersCollectionView.delegate = self
        juniorExplorersMSCollectionView.delegate = self
        juniorExplorersASCollectionView.delegate = self
    }
    
    func createSeniorExplorerArray() -> [Explorer]{
        var tempExplorer: [Explorer] = []
        
        for index in 0...SeniorExplorersData.seniorExplorers.count - 1 {
            tempExplorer.append(SeniorExplorersData.seniorExplorers[index])
            
        }
        
        return tempExplorer
    }
    
    func createJuniorExplorerMSArray() -> [Explorer]{
        var tempExplorer: [Explorer] = []
        
        for index in 0...JuniorExplorersMSData.juniorExplorersMS.count - 1 {
            tempExplorer.append(JuniorExplorersMSData.juniorExplorersMS[index])
            
        }
        
        return tempExplorer
    }
    
    func createJuniorExplorerASArray() -> [Explorer]{
        var tempExplorer: [Explorer] = []
        
        for index in 0...JuniorExplorersASData.juniorExplorersAS.count - 1 {
            tempExplorer.append(JuniorExplorersASData.juniorExplorersAS[index])
            
        }
        
        return tempExplorer
    }
}

extension ExplorersViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == seniorExplorersCollectionView) {
            return seArray.count
        } else if (collectionView == juniorExplorersMSCollectionView) {
            return jmsArray.count
        } else {
            return jasArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == seniorExplorersCollectionView) {
            let seniorExplorer = seArray[indexPath.row]
            
            let cell = seniorExplorersCollectionView.dequeueReusableCell(withReuseIdentifier: "SeniorExplorersCollectionViewCell", for: indexPath) as! SeniorExplorersCollectionViewCell
            cell.setExplorer(explorer: seniorExplorer)

            return cell
        } else if (collectionView == juniorExplorersMSCollectionView) {
            let juniorExplorerMS = jmsArray[indexPath.row]

            let cell2 = juniorExplorersMSCollectionView.dequeueReusableCell(withReuseIdentifier: "JuniorExplorersMSCollectionViewCell", for: indexPath) as! JuniorExplorersMSCollectionViewCell
            cell2.setExplorer(explorer: juniorExplorerMS)

            return cell2
        } else {
            let juniorExplorerAS = jasArray[indexPath.row]

            let cell3 = juniorExplorersASCollectionView.dequeueReusableCell(withReuseIdentifier: "JuniorExplorersASCollectionViewCell", for: indexPath) as! JuniorExplorersASCollectionViewCell
            cell3.setExplorer(explorer: juniorExplorerAS)

            return cell3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView == seniorExplorersCollectionView) {
            let seVC = storyboard?.instantiateViewController(identifier: "ExExplorerViewController") as! ExExplorerViewController
            
            seVC.explorerName = seArray[indexPath.row].name
            seVC.explorerImage = seArray[indexPath.row].image
            seVC.explorerRole = seArray[indexPath.row].role
            seVC.explorerHelp = seArray[indexPath.row].help
            seVC.explorerLearn = seArray[indexPath.row].learn
            
            self .navigationController?.pushViewController(seVC, animated: true)
        } else if (collectionView == juniorExplorersMSCollectionView) {
            if (collectionView == juniorExplorersMSCollectionView) {
                let jmsVC = storyboard?.instantiateViewController(identifier: "ExExplorerViewController") as! ExExplorerViewController
                
                jmsVC.explorerName = jmsArray[indexPath.row].name
                jmsVC.explorerImage = jmsArray[indexPath.row].image
                jmsVC.explorerRole = jmsArray[indexPath.row].role
                jmsVC.explorerHelp = jmsArray[indexPath.row].help
                jmsVC.explorerLearn = jmsArray[indexPath.row].learn
                
                self .navigationController?.pushViewController(jmsVC, animated: true)
            }
        } else if (collectionView == juniorExplorersASCollectionView) {
            let jasVC = storyboard?.instantiateViewController(identifier: "ExExplorerViewController") as! ExExplorerViewController
            
            jasVC.explorerName = jasArray[indexPath.row].name
            jasVC.explorerImage = jasArray[indexPath.row].image
            jasVC.explorerRole = jasArray[indexPath.row].role
            jasVC.explorerHelp = jasArray[indexPath.row].help
            jasVC.explorerLearn = jasArray[indexPath.row].learn
            
            self.navigationController?.pushViewController(jasVC, animated: true)
        }
    }
}
