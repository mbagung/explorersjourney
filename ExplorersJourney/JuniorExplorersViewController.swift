//
//  JuniorExplorersViewController.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class JuniorExplorersViewController: UIViewController {
    @IBOutlet weak var juniorExplorersTableView: UITableView!
    @IBOutlet weak var juniorExplorersSearchBar: UISearchBar!
    
    var jeArray: [Explorer] = []
    var selectedIndex = Int()
    
    var searchExplorer: [Explorer] = []
    var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jeArray = createJuniorExplorerArray()
        searchExplorer = createJuniorExplorerArray()
        
        juniorExplorersTableView.dataSource = self
        juniorExplorersTableView.delegate = self
        juniorExplorersSearchBar.delegate = self
    }
        
    func createJuniorExplorerArray() -> [Explorer]{
        var tempExplorer: [Explorer] = []
        var tempMS: [Explorer] = []
        var tempAS: [Explorer] = []
        
        for index in 0...JuniorExplorersMSData.juniorExplorersMS.count - 1 {
            tempMS.append(JuniorExplorersMSData.juniorExplorersMS[index])
            
        }
        
        for index in 0...JuniorExplorersASData.juniorExplorersAS.count - 1 {
            tempAS.append(JuniorExplorersASData.juniorExplorersAS[index])
            
        }
        
        tempExplorer = tempMS + tempAS
        
        return tempExplorer
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        performSegue(withIdentifier: "ExpandJuniorExplorer", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ExpandJuniorExplorer"{
            let exExplorerVC: ExExplorerViewController = segue.destination as! ExExplorerViewController
            
            if searching {
                exExplorerVC.explorerImage = searchExplorer[selectedIndex].image
                exExplorerVC.explorerName =  searchExplorer[selectedIndex].name
                exExplorerVC.explorerRole = searchExplorer[selectedIndex].role
                exExplorerVC.explorerHelp = searchExplorer[selectedIndex].help
                exExplorerVC.explorerLearn = searchExplorer[selectedIndex].learn
            } else {
                exExplorerVC.explorerImage = jeArray[selectedIndex].image
                exExplorerVC.explorerName =  jeArray[selectedIndex].name
                exExplorerVC.explorerRole = jeArray[selectedIndex].role
                exExplorerVC.explorerHelp = jeArray[selectedIndex].help
                exExplorerVC.explorerLearn = jeArray[selectedIndex].learn
            }
        }
    }
}

    extension JuniorExplorersViewController: UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if searching {
                return searchExplorer.count
            } else {
                return jeArray.count
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let juniorExplorers = jeArray[indexPath.row]
            let searchJuniorExplorers = searchExplorer[indexPath.row]
            
            let cell = juniorExplorersTableView.dequeueReusableCell(withIdentifier: "JuniorExplorersTableViewCell") as! JuniorExplorersTableViewCell
            
            if searching {
                cell.setJuniorExplorer(juniorExplorer: searchJuniorExplorers)
            } else {
                cell.setJuniorExplorer(juniorExplorer: juniorExplorers)
            }
            return cell
        }
        
    }

extension JuniorExplorersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchExplorer = jeArray.filter ({ $0.name.lowercased().prefix(searchText.count) == searchText.lowercased() })
        searching = true
        juniorExplorersTableView.reloadData()
    }
}
