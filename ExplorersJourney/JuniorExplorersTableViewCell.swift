//
//  JuniorExplorersTableViewCell.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

class JuniorExplorersTableViewCell: UITableViewCell {
    @IBOutlet weak var juniorExplorerImageView: UIImageView!
    @IBOutlet weak var juniorExplorerNameLabel: UILabel!
    @IBOutlet weak var juniorExplorerRoleLabel: UILabel!
    @IBOutlet weak var juniorExplorerShiftLabel: UILabel!
    

func setJuniorExplorer(juniorExplorer: Explorer) {
    juniorExplorerImageView.image = juniorExplorer.image
    juniorExplorerNameLabel.text = juniorExplorer.name
    juniorExplorerRoleLabel.text = juniorExplorer.role
    juniorExplorerShiftLabel.text = juniorExplorer.shift
    }
}
