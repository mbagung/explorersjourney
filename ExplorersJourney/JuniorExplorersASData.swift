//
//  JuniorExplorersASData.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 22/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import UIKit

struct JuniorExplorersASData {
    static var juniorExplorersAS: [Explorer] =
    [
        Explorer(name: "Almas Sharfina Puspandam", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Almas_AF"), learn: "", help: ""),
        Explorer(name: "Harvey Lienardo", seniority: "Junior", shift: "Afternoon", role: "Design", image:#imageLiteral(resourceName: "Harvey_AF"), learn: "", help: "Photography, image processing, layout"),
        Explorer(name: "Jeslyn Kosasih", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Jeslyn_AF"), learn: "", help: ""),
        Explorer(name: "Jessica Ayumi Aris", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Ayumi_AF"), learn: "", help: ""),
        Explorer(name: "Kezia Lauren Handoyo", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Kezia_AF"), learn: "", help: ""),
        Explorer(name: "Livia Angelica Gunawan", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Livia_AF"), learn: "", help: "Basic graphic design stuff (Photoshop, Illustrator), English maybe hehe, copywriting"),
        Explorer(name: "Michelle Caroline Kristanto", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Michelle_AF"), learn: "", help: "Graphic Design (Layouting)"),
        Explorer(name: "Samantha Teonata", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Sam_AF"), learn: "", help: "Graphic Design, bonding and getting along with people, ngeramein acara (BUKAN badut ultah), boosting your confidence (if you feel like you need one), ngobrol dan ngoceh"),
        Explorer(name: "Tivany Hartono", seniority: "Junior", shift: "Afternoon", role: "Design", image: #imageLiteral(resourceName: "Tivany_AF"), learn: "", help: "Graphic Design"),
        Explorer(name: "Angelica Irene Christina", seniority: "Junior", shift: "Afternoon", role: "Other", image: #imageLiteral(resourceName: "Ren_AF"), learn: "", help: ""),
        Explorer(name: "Jessy The Wirianto", seniority: "Junior", shift: "Afternoon", role: "Other", image: #imageLiteral(resourceName: "Jessy_AF"), learn: "Coding\nDesign\nCommunication Skill", help: "Business Idea\nDesign Input"),
        Explorer(name: "Krisna Harimurti", seniority: "Junior", shift: "Afternoon", role: "Other", image: #imageLiteral(resourceName: "Krisna_AF"), learn: "", help: ""),
        Explorer(name: "Stefany", seniority: "Junior", shift: "Afternoon", role: "Other", image: #imageLiteral(resourceName: "Stefany_AF"), learn: "pengen sih bisa gambar wkwk", help: ""),
        Explorer(name: "Victor Wijaya Ngantung", seniority: "Junior", shift: "Afternoon", role: "Other", image: #imageLiteral(resourceName: "Victor_AF"), learn: "Coding", help: "Business\nDesign Basics"),
        Explorer(name: "Abraham Christopher", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Abraham_AF"), learn: "", help: ""),
        Explorer(name: "Afitra Mamor Bikhoir", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Afitra_AF"), learn: "how to implement crud in swift", help: "Basic Coding"),
        Explorer(name: "Alessandro Vieri", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Aldo_AF"), learn: "", help: ""),
        Explorer(name: "Andre Marines Ado Tena Uak", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Matu_AF"), learn: "", help: ""),
        Explorer(name: "Angelina Chandra", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Angel_AF"), learn: "", help: ""),
        Explorer(name: "Arinal Haq", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Arinal_AF"), learn: "", help: ""),
        Explorer(name: "Auriga Aristo", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Riga_AF"), learn: "", help: ""),
        Explorer(name: "Candra Sabdana Nugroho", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Candra_AF"), learn: "", help: ""),
        Explorer(name: "Christian Tanjono", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Christian_AF"), learn: "- Advance OOP\n- UI Design\n- Transition on Swift (Onboarding)", help: "- Basic Coding\n- Basic Programming Algorithm\n- Network Troubleshooting\n- Achieving your Body Goals"),
        Explorer(name: "Christopher Kevin Susandji", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Kikis_AF"), learn: "SQLite CRUD :(((", help: "Basic Swift, Basic Coding, Basic UI and Graphic Design, software-related stuffs (e.g. installing Minecraft, uninstalling Fortnite)"),
        Explorer(name: "David Christian Kartamihardja", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "David_AF"), learn: "HIG, Layouting, design, UI/UX, English presentation", help: "Basic swift, basic coding"),
        Explorer(name: "Dimarta Julkha Faradiba", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Marta_AF"), learn: "", help: ""),
        Explorer(name: "Fauzia Umar", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Zizi_AF"), learn: "", help: "basic coding, basic swift, UIUX"),
        Explorer(name: "Felicia Gunadi", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Felicia_AF"), learn: "Code, Presentation", help: "Basic Coding, UI Design, Team Management"),
        Explorer(name: "I Putu Andhika Indrastata Widyadhana", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Putu_AF"), learn: "Take image data from taken photos", help: "-Basic Coding, Basic Sketch\n-Having Fun"),
        Explorer(name: "Ivan winarto", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Ivan_AF"), learn: "present properly in english", help: "Basic Coding\nTeam management"),
        Explorer(name: "Jeremy Endratno", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Jeremy_AF"), learn: "", help: ""),
        Explorer(name: "Joahan Wirasugianto", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Joahan_AF"), learn: "- Code\n- UI Design", help: "Code, UI Design, Get Reletionship, Achieving your Body Goals"),
        Explorer(name: "Jovan Alvin", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Jovan_AF"), learn: "", help: ""),
        Explorer(name: "Kevin Heryanto", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "KH_AF"), learn: "Code, Design, HIG, English Speaking", help: "Code, Makan-Makan."),
        Explorer(name: "Kevin Pratama Soedarso", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Kp_AF"), learn: "", help: ""),
        Explorer(name: "Moch Maulana Ardiansyah", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Maul_AF"), learn: "", help: "Basic Swift, UI/UX Design, Graphic Design, Layouting, Coding, Logical Thinking"),
        Explorer(name: "Monica Adelya Putri", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Monica_AF"), learn: "", help: ""),
        Explorer(name: "Muhammad Bangun Agung", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Agung_AF"), learn: "Presentation, Collaboration", help: "Coding, UI Design, Animation"),
        Explorer(name: "Muhammad Faisal Febrianto", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Faisal_AF"), learn: "ui,ux and english speaking", help: "basic coding"),
        Explorer(name: "Muhammad Naufal Muzaky Sukmana Putra", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Zaky_AF"), learn: "", help: ""),
        Explorer(name: "Natasha Adeline Wardhana", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Tata_AF"), learn: "", help: ""),
        Explorer(name: "Richard Santoso", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Richard_AF"), learn: "", help: "basic coding, English"),
        Explorer(name: "Ricky Austin Setiabudi", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Ricky_AF"), learn: "", help: "Layouting, Logical Thinking, Basic Swift, photo manipulation, and pushing your score in the game"),
        Explorer(name: "Rini Handini", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Rini_AF"), learn: "", help: ""),
        Explorer(name: "Ryan Anslyno Khohari", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Ryan_AF"), learn: "Code with Model View", help: "Basic Coding"),
        Explorer(name: "Silviera Dewi Nagari", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Silviera_AF"), learn: "", help: ""),
        Explorer(name: "Timotius Cahyadi", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Timotius_AF"), learn: "", help: "Basic Coding, Basic Design"),
        Explorer(name: "Timotius Leonardo Lianoto", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Timol_AF"), learn: "Design and how to pick a good colour collaboration", help: "Code Logic, Public Speaking"),
        Explorer(name: "Ulinnuha Nabilah", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Nabilah_AF"), learn: "", help: ""),
        Explorer(name: "Wienona Martha Parlina", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "Wienona_AF"), learn: "", help: ""),
        Explorer(name: "William Sebastian Thedja", seniority: "Junior", shift: "Afternoon", role: "Tech", image: #imageLiteral(resourceName: "William_AF"), learn: "", help: ""),
    ]
}

//#imageLiteral(resourceName: "Januar_SE" <#T##String#>)
