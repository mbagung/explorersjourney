//
//  Challenge.swift
//  ExplorersJourney
//
//  Created by Muhammad Bangun Agung on 17/04/20.
//  Copyright © 2020 Muhammad Bangun Agung. All rights reserved.
//

import Foundation
import UIKit

class Challenge {
    var image: UIImage
    var title: String
    var category: String
    var duration: String
    var description: String
    var learningGoals: String
    var constraints: String
    var deliverables: String
    
    init(image: UIImage, title: String, category: String, duration: String, description: String, learningGoals: String, constraints: String, deliverables: String) {
        self.image = image
        self.title = title
        self.category = category
        self.duration = duration
        self.description = description
        self.learningGoals = learningGoals
        self.constraints = constraints
        self.deliverables = deliverables
    }
}
